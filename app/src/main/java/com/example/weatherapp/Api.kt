package com.example.weatherapp
import android.util.Log
import kotlinx.coroutines.*
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import java.util.Calendar


class WeatherResponse(val cod: Int, val name: String, val main: WeatherData){
    override fun toString(): String {
        val str : String
        if (cod == 200)
            str =  name + ": " + main.temp.toString()
        else str = cod.toString()
        return str
    }

    fun toWeather(): Weather {
        return Weather(0, name, Calendar.getInstance().timeInMillis, main.temp)
    }
}

class WeatherData(
    val temp : Float
)

interface WeatherApi {
    @GET("/data/2.5/weather")
    suspend fun getTempByName(@Query("appid") appid: String,
                              @Query("q") location: String)
            : WeatherResponse;

    @GET("/data/2.5/weather")
    suspend fun getTempByLoc(@Query("appid") appid: String,
                             @Query("lat") lat: String,
                             @Query("lon") lng: String)
            : WeatherResponse;
}

class WeatherAPIHelper(){
    private val weatherApi: WeatherApi

    init {
        val retrofit = Retrofit.Builder()
            .baseUrl("https://api.openweathermap.org")
            .addConverterFactory(MoshiConverterFactory.create())
            .build()

        weatherApi = retrofit.create(WeatherApi::class.java)
    }

    suspend fun getTempByName(appid: String, location: String): WeatherResponse{
        return weatherApi.getTempByName(appid, location)
    }

    suspend fun getTempByLoc(appid: String, lat: String, lng: String): WeatherResponse{
        return weatherApi.getTempByLoc(appid, lat, lng)
    }
}

class WeatherManager(){
    private val api: WeatherAPIHelper = WeatherAPIHelper()
    private val scope = CoroutineScope(Dispatchers.Unconfined)
    lateinit var resp: WeatherResponse

    suspend fun getTempByName(appid: String, location: String): Weather?{
        val result = api.getTempByName(appid, location)
        if (result != null) {
            Log.d("Weather: ", result.toString())
            resp = result
            return resp.toWeather()
        }
        return null
    }

    suspend fun getTempByLoc(appid: String, lat: String, lng:String): Weather?{
        val result = api.getTempByLoc(appid, lat, lng)
        if (result != null) {
            Log.d("Weather: ", result.toString())
            resp = result
            return resp.toWeather()
        }
        return null
    }

}