package com.example.weatherapp

import android.Manifest
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.ImageButton
import android.widget.SearchView
import android.widget.Switch
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.Date


class MainActivity : AppCompatActivity() {
    private val appid = "insertYoutKeyHere"

    //    private lateinit var cityViewModel: CityViewModel
    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter: CityAdapter
    private var currentWeather: Weather? = null
    private val scope = CoroutineScope(Dispatchers.Unconfined)
    private var location: Location? = null
    private lateinit var locationManager: LocationManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val weatherManager = WeatherManager()

        val db = Room.databaseBuilder(
            applicationContext,
            WeatherDatabase::class.java, "database-name"
        ).fallbackToDestructiveMigration().build()

        adapter = CityAdapter(
            listOf()
        )


        recyclerView = findViewById<RecyclerView>(R.id.locationRecycle)
        recyclerView.layoutManager = LinearLayoutManager(this)

        recyclerView.adapter = adapter


        // Тут сетапим mock данные, чтобы что-то сразу было
        scope.launch{
            withContext(Dispatchers.IO){
                var cities = db.weatherDao().getAll()
                if(cities.isEmpty())
                    db.weatherDao().insertAll(Weather(0, "Moscow", 1701869799000, 300.0f),
                        Weather(0, "London", 1701849799000, 284.15f),
                        Weather(0, "Petrozavodsk", 1701869549000, 254.25f))
                cities = db.weatherDao().getAll()
                MainScope().launch {
                    adapter.setCitiesFromValues(cities)
                }
            }
        }

        val fahrOrCelsSwitch = findViewById<Switch>(R.id.fahrCelsSwitch)
        val buttonClick = findViewById<ImageButton>(R.id.currentLocBtn)
        buttonClick.setOnClickListener(object : View.OnClickListener {
            override fun onClick(view: View?) {
                println("Что-то запустилось")
                if(!checkLocationPermission())
                    return
                locationManager = applicationContext.getSystemService(LOCATION_SERVICE) as LocationManager
                location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
                scope.launch{
                    withContext(Dispatchers.IO){
                        currentWeather = weatherManager.getTempByLoc(appid, location?.latitude.toString(), location?.longitude.toString())
                        val w = db.weatherDao().findByName(currentWeather!!.txtCityName!!)
                        if (w == null)
                            db.weatherDao().insertAll(currentWeather!!)
                        else db.weatherDao().updateOne(currentWeather!!)
                        val cities = db.weatherDao().getAll()
                        MainScope().launch {
                            adapter.setCitiesFromValues(cities)
                            parseCurrentWeather(fahrOrCelsSwitch.isChecked)
                        }
                    }
                }
            }
        })

        fahrOrCelsSwitch.setOnCheckedChangeListener {
                _: CompoundButton, isOn: Boolean ->
            adapter.setCelsiusOn(isOn)
            parseCurrentWeather(isOn)
            adapter.notifyItemRangeChanged(0,adapter.itemCount)
        }
        val search = findViewById<SearchView>(R.id.searchView)
        search.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextChange(newText: String): Boolean {
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                scope.launch{
                    withContext(Dispatchers.IO){
                        currentWeather = weatherManager.getTempByName(appid, query)
                        val w = db.weatherDao().findByName(currentWeather!!.txtCityName!!)
                        if (w == null) {
                            db.weatherDao().insertAll(currentWeather!!)
                            println("Inserted")
                        }
                        else{
                            db.weatherDao().updateOne(Weather(w.intWeatherId, w.txtCityName,
                                currentWeather!!.longDate, currentWeather!!.floatKelvinTemp))
                            println("Updated")
                        }
                        val cities = db.weatherDao().getAll()
                        println(cities)
                        MainScope().launch {
                            adapter.setCitiesFromValues(cities)
                            parseCurrentWeather(fahrOrCelsSwitch.isChecked)
                        }
                    }
                }
                return false
            }

        })
    }

    fun parseCurrentWeather(celsiusOn: Boolean){
        val mainView = findViewById<CardView>(R.id.mainView)
        val cityName = findViewById<TextView>(R.id.cityNameText)
        val temperatureText = findViewById<TextView>(R.id.TempText)
        if (currentWeather == null)
            return
        val celsTemp = currentWeather!!.floatKelvinTemp -273.15f
        when{
            celsTemp < 10f -> mainView.setCardBackgroundColor(ContextCompat.getColor(this, R.color.lightblue500))
            celsTemp in 10f..25f -> mainView.setCardBackgroundColor(ContextCompat.getColor(this, R.color.orange500))
            celsTemp > 25f -> mainView.setCardBackgroundColor(ContextCompat.getColor(this, R.color.red500))
        }

        cityName.text = currentWeather!!.txtCityName
        if(celsiusOn)
            temperatureText.text  = String.format("%.1f°C",celsTemp)
        else
            temperatureText.text  = String.format("%.1f°F", (celsTemp)*1.8f + 32f)
    }

    private fun checkLocationPermission() : Boolean
    {
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
            || ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION), 15)
            return false
        }
        else return true
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if(requestCode == 15){
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                return
            }
        }
    }


}

class CityAdapter(private val values: List<Weather>) : RecyclerView.Adapter<CityAdapter.ViewHolder>()
{
    private var cities: List<Weather>
    private val obj: DateFormat = SimpleDateFormat("dd.MM.yyyy HH:mm:ss ")

    private var celsiusOn = false

    private val celsiusDegreeList = mutableListOf<Float>()
    private val fahrenheitDegreeList = mutableListOf<Float>()

    init {
        cities = listOf()
        setCitiesFromValues(values)
    }
    fun setCelsiusOn(on: Boolean){
        celsiusOn = on
    }
    fun setCitiesFromValues(values: List<Weather>){//get the current items
        //get the current items
        val currentSize: Int = cities.size
        //add all the new items
        cities = values
        //tell the recycler view that all the old items are gone
        notifyItemRangeRemoved(0, currentSize)
        //tell the recycler view how many new items we added
        notifyItemRangeInserted(0, cities.size)
        cities = values
        celsiusDegreeList.removeAll(celsiusDegreeList)
        fahrenheitDegreeList.removeAll(fahrenheitDegreeList)
        for (i in values){
            celsiusDegreeList.add(i.floatKelvinTemp -273.15f)
            fahrenheitDegreeList.add((i.floatKelvinTemp -273.15f)*1.8f + 32f)
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.recyclerview_row, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return cities.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.cityName?.text = cities[position].txtCityName
        if(celsiusOn)
            holder.cityName?.text  = String.format("%s, %.1f°C", cities[position].txtCityName, celsiusDegreeList[position])
        else
            holder.cityName?.text  = String.format("%s, %.1f°F", cities[position].txtCityName, fahrenheitDegreeList[position])
        holder.weatherDate?.text = obj.format(Date(cities[position].longDate))
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        var cityName: TextView? = null
        var weatherDate: TextView? = null
        init {
            cityName = itemView.findViewById(R.id.cityName)
            weatherDate = itemView.findViewById(R.id.weatherDate)
        }
    }
}