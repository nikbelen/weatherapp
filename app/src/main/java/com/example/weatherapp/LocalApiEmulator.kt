package com.example.weatherapp

import android.util.Log
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApiLocal {
    @GET("weather")
    suspend fun getTempByName(@Query("appid") appid: String,
                              @Query("q") location: String)
            : WeatherResponse;

    @GET("weather")
    suspend fun getTempByLoc(@Query("appid") appid: String,
                             @Query("lat") lat: String,
                             @Query("lon") lng: String)
            : WeatherResponse;
}

class WeatherAPIHelperLocal(){
    private val weatherApi: WeatherApiLocal

    init {
        val retrofit = Retrofit.Builder()
            .baseUrl("http://10.0.2.2:5000")
            .addConverterFactory(MoshiConverterFactory.create())
            .build()

        weatherApi = retrofit.create(WeatherApiLocal::class.java)
    }

    suspend fun getTempByName(appid: String, location: String): WeatherResponse{
        return weatherApi.getTempByName(appid, location)
    }

    suspend fun getTempByLoc(appid: String, lat: String, lng: String): WeatherResponse{
        return weatherApi.getTempByLoc(appid, lat, lng)
    }
}

class WeatherManagerLocal(){
    private val api: WeatherAPIHelperLocal = WeatherAPIHelperLocal()
    private val scope = CoroutineScope(Dispatchers.Unconfined)
    lateinit var resp: WeatherResponse

    suspend fun getTempByName(appid: String, location: String): Weather?{
        val result = api.getTempByName(appid, location)
        if (result != null) {
            Log.d("Weather: ", result.toString())
            resp = result
            return resp.toWeather()
        }
        return null
    }

    suspend fun getTempByLoc(appid: String, lat: String, lng:String): Weather?{
        val result = api.getTempByLoc(appid, lat, lng)
        if (result != null) {
            Log.d("Weather: ", result.toString())
            resp = result
            return resp.toWeather()
        }
        return null
    }

}
