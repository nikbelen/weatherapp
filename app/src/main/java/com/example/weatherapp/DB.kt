package com.example.weatherapp
import androidx.room.*
import java.util.*

@Entity
data class Weather(
    @PrimaryKey(autoGenerate = true) var intWeatherId: Int,
    @ColumnInfo(name = "txtCityName") var txtCityName: String?,
    @ColumnInfo(name = "longDate") var longDate: Long,
    @ColumnInfo(name = "floatKelvinTemp") var floatKelvinTemp: Float
)

@Dao
interface WeatherDao {
    @Query("SELECT * FROM weather")
    fun getAll(): List<Weather>

    @Query("SELECT * FROM weather WHERE txtCityName LIKE :cityName" +
            " LIMIT 1")
    fun findByName(cityName: String): Weather

    @Insert
    fun insertAll(vararg weather: Weather)

    @Update
    fun updateOne(weather: Weather)
    @Delete
    fun delete(weather: Weather)


}

@Database(entities = [Weather::class], version = 1, exportSchema = false)
abstract class WeatherDatabase : RoomDatabase() {
    abstract fun weatherDao(): WeatherDao
}